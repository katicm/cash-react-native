import React, { Component } from "react";
import { createBottomTabNavigator } from "react-navigation";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import PrimaryScreen from "./components/screens/PrimaryScreen";
import SecondaryScreen from "./components/screens/SecondaryScreen";
import { store, persistor } from "./components/redux/store";

const RootNavigator = createBottomTabNavigator(
  {
    Primary: PrimaryScreen,
    Secondary: SecondaryScreen
  },
  {
    tabBarOptions: {
      activeTintColor: "#FFFFFF",
      activeBackgroundColor: "#012a6d",
      inactiveBackgroundColor: "#012a6d",
      showLabel: false
    }
  }
);
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootNavigator />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
