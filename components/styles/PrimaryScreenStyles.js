import { StyleSheet } from "react-native";

export default (styles = StyleSheet.create({
  container: {
    backgroundColor: "#ededed"
  },
  firstRow: {
    padding: 20,
    paddingRight: 40,
    alignItems: "baseline",
    flexDirection: "row",
    justifyContent: "space-between",
    fontSize: 30
  },
  secondRow: {
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 40,
    alignItems: "baseline",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  rowView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "baseline",
    marginRight: 20,
    marginLeft: 15
  },
  listRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  inputRow: {
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#278ff7"
  },
  inputField: {
    flex: 1,
    alignSelf: "center",
    fontWeight: "300",
    fontSize: 17
  },
  buttonRed: {
    backgroundColor: "#e20000",
    width: 40,
    height: 40
  },
  buttonGreen: {
    backgroundColor: "#007c0e",
    width: 40,
    height: 40
  }
}));
