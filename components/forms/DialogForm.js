import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Dialog from "react-native-dialog";

export default class DialogForm extends Component {
  state = {
    initialSaldo: ""
  };
  handleInput = initialSaldo => {
    this.setState({ initialSaldo });
  };
  handleCancel = () => {
    this.props.hideDialog();
  };
  handleSubmit = () => {
    this.props.onSubmitInitial(this.state.initialSaldo);
  };

  render() {
    return (
      <Dialog.Container visible={true}>
        <Dialog.Title>Set the initial money balance</Dialog.Title>
        <Dialog.Input
          placeholder="RSD"
          autoFocus={true}
          onChangeText={this.handleInput}
          keyboardType="numeric"
        />
        <Dialog.Button label="Cancel" onPress={this.handleCancel} />
        <Dialog.Button label="Confirm" onPress={this.handleSubmit} />
      </Dialog.Container>
    );
  }
}
