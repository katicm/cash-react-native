import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import Dialog from "react-native-dialog";

export default class DialogAlertForm extends Component {
  handleCancel = () => {
    this.setState({ visible: false });
  };
  handleSubmit = () => {
    this.props.onShowDialog();
  };

  render() {
    return (
      <Dialog.Container visible={true}>
        <Dialog.Title>Set the initial money balance</Dialog.Title>
        <Dialog.Button label="Confirm" onPress={this.handleSubmit} />
      </Dialog.Container>
    );
  }
}
