import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TextInput,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { addCost, deleteCost, addInitialMoney } from "../redux/actions";
import Ionicons from "react-native-vector-icons/Ionicons";
import { Header, List, ListItem } from "react-native-elements";
import Button from "apsl-react-native-button";
import isInt from "validator/lib/isInt";
import DialogForm from "../forms/DialogForm";
import DialogAlertForm from "../forms/DialogAlertForm";
import styles from "../styles/PrimaryScreenStyles";

class PrimaryScreen extends Component {
  state = {
    product: "",
    price: "",
    visibleDialog: false,
    isDataValid: false
  };
  handleRefresh = () => {
    this.setState({ visibleDialog: true });
  };
  hideDialog = () => {
    this.setState({ visibleDialog: false });
  };
  onSubmitInitial = initialMoney => {
    this.setState({ visibleDialog: false });
    this.props.addInitialMoney(initialMoney);
  };
  componentDidUpdate(prevProps, prevState) {
    if (
      this.state.product !== prevState.product ||
      this.state.price !== prevState.price
    ) {
      this.validateData();
    }
  }
  validateData = () => {
    if (this.state.product.length >= 3 && isInt(this.state.price)) {
      this.setState({ isDataValid: true });
    } else {
      this.setState({ isDataValid: false });
    }
  };
  handlePriceInput = price => {
    this.setState({ price });
  };
  handleProductInput = product => {
    this.setState({ product });
  };
  handleDelete = (id, price) => {
    this.props.deleteCost({ id, price });
  };
  onSubmit = () => {
    this.props.addCost(this.state.product, this.state.price);
    this.setState({ product: "", price: "" });
  };
  static navigationOptions = {
    tabBarIcon: ({ tintColor }) => (
      <Ionicons name="md-cash" size={35} color={tintColor} />
    )
  };
  render() {
    return (
      <ScrollView style={styles.container}>
        <Header
          centerComponent={{
            text: "CA$H Manager",
            style: { color: "#fff", fontSize: 30 }
          }}
          rightComponent={
            <Ionicons
              name="md-refresh"
              size={30}
              onPress={this.handleRefresh}
            />
          }
        />
        {this.state.visibleDialog && (
          <DialogForm
            hideDialog={this.hideDialog}
            onSubmitInitial={this.onSubmitInitial}
          />
        )}
        {!this.props.money.initial && (
          <DialogAlertForm onShowDialog={this.handleRefresh} />
        )}
        <View style={styles.firstRow}>
          <Text style={{ fontSize: 20 }}>Current amount:</Text>
          <Text style={{ fontSize: 40, textAlign: "left" }}>
            {this.props.money.saldo}
          </Text>
        </View>
        <View style={styles.secondRow}>
          <Text style={{ fontSize: 15 }}>Initial amount:</Text>
          <Text style={{ fontSize: 20, textAlign: "left" }}>
            {this.props.money.initial}
          </Text>
        </View>
        <View style={{ height: 330, marginBottom: 20 }}>
          <ScrollView>
            <List>
              {this.props.data
                .slice(0)
                .reverse()
                .map(item => (
                  <ListItem
                    key={item.id}
                    hideChevron={true}
                    subtitle={
                      <View style={styles.rowView}>
                        <Text style={styles.listRow}>{item.product}</Text>
                        <Text style={styles.listRow}>{item.price}</Text>
                        <Ionicons
                          onPress={this.handleDelete.bind(
                            this,
                            item.id,
                            item.price
                          )}
                          name="md-trash"
                          size={24}
                        />
                      </View>
                    }
                  />
                ))}
            </List>
          </ScrollView>
        </View>
        <KeyboardAvoidingView behavior="padding" style={styles.inputRow}>
          <TextInput
            value={this.state.product}
            style={styles.inputField}
            placeholder="Enter product"
            onChangeText={this.handleProductInput}
          />
          <TextInput
            value={this.state.price}
            style={styles.inputField}
            placeholder="Enter price"
            onChangeText={this.handlePriceInput}
            keyboardType="numeric"
          />
          <View style={{ marginTop: 10 }}>
            <Button
              style={styles.buttonGreen}
              disabledStyle={styles.buttonRed}
              isDisabled={!this.state.isDataValid}
              textStyle={{ fontSize: 40 }}
              onPress={this.onSubmit}
            >
              +
            </Button>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  data: state.data,
  money: state.money
});

export default connect(
  mapStateToProps,
  { addCost, deleteCost, addInitialMoney }
)(PrimaryScreen);
