import { ADD_ITEM, DELETE_COST, INITIAL_MONEY } from "../redux/types";
import { combineReducers } from "redux";

const dataReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_ITEM: {
      return [...state, action.payload];
    }
    case DELETE_COST: {
      return state.filter(element => element.id !== action.payload.id);
    }
    case INITIAL_MONEY: {
      return [];
    }
    default:
      return state;
  }
};
const moneyReducer = (state = {}, action) => {
  switch (action.type) {
    case ADD_ITEM: {
      return { ...state, saldo: state.saldo - action.payload.price };
    }
    case DELETE_COST: {
      return { ...state, saldo: +state.saldo + +action.payload.price };
    }
    case INITIAL_MONEY: {
      return { ...state, saldo: action.saldo, initial: action.saldo };
    }
    default:
      return state;
  }
};
export const reducer = combineReducers({
  data: dataReducer,
  money: moneyReducer
});
