import { ADD_ITEM, DELETE_COST, INITIAL_MONEY } from "./types";
import shortid from "shortid";

export const deleteCost = payload => ({
  type: DELETE_COST,
  payload
});
export const addInitialMoney = saldo => ({
  type: INITIAL_MONEY,
  saldo
});
export const addCost = (product, price) => ({
  type: ADD_ITEM,
  payload: { product, price, id: shortid.generate() }
});
